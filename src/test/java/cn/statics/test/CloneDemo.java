package cn.statics.test;

public class CloneDemo implements Cloneable {

	int x;
	int y;

	public static void cloneObj() throws CloneNotSupportedException {
		CloneDemo dm = new CloneDemo();
		dm.x = 123;
		dm.y = 456;

		System.out.println("dm.x=%d%n"+dm.x+"/"+dm.y);
		
		CloneDemo cd = (CloneDemo) dm.clone();
		System.out.print("cd.x=%d%n"+cd.x+"/"+cd.y);
	}
	
	public static void main(String[] args) {
		try {
			cloneObj();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
	}
}
