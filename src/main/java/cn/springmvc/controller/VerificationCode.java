package cn.springmvc.controller;

import cn.springmvc.comm.RandomValidateCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * springmvc
 * cn.springmvc.controller
 * Created by 刘桃荣 on 2015/1/19.
 * 返回验证码
 */
@Controller
public class VerificationCode {



    @RequestMapping(value = "/user/verificationCode",produces = "image/jpeg")
    public void getVerificationCode(HttpServletRequest request,
                                    HttpServletResponse response){
        RandomValidateCode validateCode=new RandomValidateCode();
        validateCode.getRandcode(request,response);
    }
}
