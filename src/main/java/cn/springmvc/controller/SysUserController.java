package cn.springmvc.controller;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import cn.springmvc.model.SysUser;

import cn.springmvc.service.SysUserService;
import cn.springmvc.service.impl.SysMenuServiceImpl;
import cn.springmvc.service.impl.SysUserServiceImpl;
import net.sf.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import cn.springmvc.comm.GlobalConstants;
import cn.springmvc.interceptor.Page;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.WebUtils;

@Controller
@RequestMapping("/sys")
public class SysUserController {

	@Autowired
	protected SysMenuServiceImpl sysMenuService;


	@Autowired
	protected SysUserServiceImpl sysUserService;
/**
 * 登录
 * @param request
 * @param user
 * @return
 */
	@RequestMapping("/user/longIn")
	public String longIn(HttpServletRequest request,SysUser user ,Model model) {
		System.out.println("userName:"+user.getUserName());
//		System.out.println("passWord:"+user.getPassWord());
		WebUtils.setSessionAttribute(request,GlobalConstants.SESSION_LOGIN_USER,user);

		String menus=sysMenuService.getMenus();
//		model.addAttribute("SYS_MENUS",menus);
		WebUtils.setSessionAttribute(request, GlobalConstants.SESSION_SYS_PATH,sysUserService.getSystem());

		WebUtils.setSessionAttribute(request, GlobalConstants.SESSION_SYS_MENU, menus);
		return "index";
	}
	
	/**
	 * 退出
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "user/loginout", method = RequestMethod.GET)
	public String loginOut(HttpServletRequest request) {

		// 清除session
		Enumeration<String> em = request.getSession().getAttributeNames();
		while (em.hasMoreElements()) {
			request.getSession().removeAttribute(em.nextElement().toString());
		}
		request.getSession().removeAttribute(GlobalConstants.SESSION_LOGIN_USER);
		request.getSession().invalidate();
		return "redirect:/login.html";

	}


	/**
	 * 根据邮箱找回密码
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "forgot/password", method = RequestMethod.GET)
	public String sendMail(HttpServletRequest request) {


		return "redirect:/login.html";

	}


	@RequestMapping("/userList")
	@ResponseBody
	public Map getUser(HttpSession session, HttpServletRequest request) {
		Page<SysUser> page = new Page<SysUser>();
		page.setPageSize(10);
		page.setPageNo(1);
		List<SysUser> list = sysUserService.findAll(page);
		
		Map map =new HashMap();
		map.put("total", page.getTotalRecord());
		map.put("rows", list);
		
		JSONArray json = new JSONArray();
		json.add(map);
		json.listIterator();
		return map;
	}

	@RequestMapping("/home")
	public String home(HttpSession session, HttpServletRequest request) {
//		Page<SysUser> page = new Page<SysUser>();
		/*page.setPageSize(no);
		page.setPageNo(size);*/
//		List<SysUser> list = userService.finaAll(page);

//		Map map =new HashMap();
		/*map.put("total", page.getTotalRecord());
		map.put("rows", list);

		JSONArray json = new JSONArray();
		json.add(map);
		json.listIterator();*/
		return "/sys/sysUserList";
	}


	@RequestMapping(value="user/addUser",method=RequestMethod.POST)
	public String addDict(SysUser user) throws Exception{
//		userService.insertUser(user);
		return "../../SysUser/index";
	}
}
