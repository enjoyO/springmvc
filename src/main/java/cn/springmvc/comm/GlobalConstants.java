package cn.springmvc.comm;

public  class  GlobalConstants {
	//放到登陆里面的用户session key
	public static String SESSION_LOGIN_USER="USER";

	//验证码session key
	public static String SESSION_VERIFICATION_CODE="VERIFICATION_CODE";

	public static String SESSION_SYS_MENU="SYS_MENUS"; //菜单

	public static String SESSION_SYS_PATH="SYS_PATH";//环境信息
}
