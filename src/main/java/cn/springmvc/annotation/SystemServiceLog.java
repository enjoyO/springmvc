package cn.springmvc.annotation;

import java.lang.annotation.*;

/**
 * springmvc
 * cn.springmvc.annotation
 * Created by 刘桃荣 on 2014/12/30.
 * describe:自定义注解 拦截Service
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.PARAMETER,ElementType.METHOD})
@Documented
public @interface SystemServiceLog {

    String description() default "";
}
