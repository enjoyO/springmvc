package cn.springmvc.freemarker;

import freemarker.cache.TemplateLoader;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import java.util.List;

/**
 * springmvc
 * cn.springmvc.freemarker
 * Created by 刘桃荣 on 2015/1/21.
 */
public class CustomFreeMarkerConfigurer extends FreeMarkerConfigurer
{
    @Override
    protected TemplateLoader getAggregateTemplateLoader(List<TemplateLoader> templateLoaders) {
       /* HtmlTemplateLoader 这个类用来自定义html模板*/
        return new HtmlTemplateLoader(super.getAggregateTemplateLoader(templateLoaders));
    }
}
