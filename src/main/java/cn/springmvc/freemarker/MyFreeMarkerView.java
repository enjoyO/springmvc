package cn.springmvc.freemarker;

import org.springframework.web.servlet.view.freemarker.FreeMarkerView;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * Created by Lenovo on 2015/9/29.
 */
public class MyFreeMarkerView extends FreeMarkerView {

    private static final String CONTEXT_PATH = "base";
    @Override
    protected void exposeHelpers(Map<String, Object> model,
                                 HttpServletRequest request) throws Exception {
        model.put(CONTEXT_PATH, request.getContextPath());
        System.out.println("==============="+model.get(CONTEXT_PATH));
        super.exposeHelpers(model, request);
    }
}
