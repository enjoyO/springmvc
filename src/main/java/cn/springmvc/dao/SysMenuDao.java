package cn.springmvc.dao;


import cn.springmvc.model.SysMenu;

import java.util.List;

/**
 * Created by Lenovo on 2015/9/28.
 */
public interface SysMenuDao extends BaseDAO {

    public List<SysMenu> getSysMenu(SysMenu sysMenu);
}
