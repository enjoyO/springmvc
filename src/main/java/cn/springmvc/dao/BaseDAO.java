package cn.springmvc.dao;

import java.util.List;

import cn.springmvc.interceptor.Page;

public interface BaseDAO<T> {

//    public void insert(T entity) throws Exception;

    public Integer insert(T entity);

    public int insertBatch(List<T> entitys);

    public int delete(T entity);

    public int deleteById(Long id);

    public List<T> findAll();

    public List<T> findAll(Page<T> page);

    public T findById(Long id);

    public T find(T entity);

}
