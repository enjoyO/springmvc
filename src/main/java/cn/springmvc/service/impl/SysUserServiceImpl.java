package cn.springmvc.service.impl;

import cn.springmvc.dao.SysUserDao;
import cn.springmvc.interceptor.Page;
import cn.springmvc.model.SysUser;
import cn.springmvc.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.*;

/**
 * Created by Lenovo on 2015/9/28.
 */
@Service
public class SysUserServiceImpl{

    @Autowired
    private SysUserDao sysUserDao;

    public SysUser find(SysUser sysUser) {
        return sysUserDao.find(sysUser);
    }

    public List<SysUser> findAll(Page<SysUser> page){
        return sysUserDao.findAll(page);
    }

    public Map<String,String> getSystem()  {
        Map<String,String> map= new LinkedHashMap<String, String>();
        InetAddress address = null;
        try {
            address = InetAddress.getLocalHost();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        String osUser=System.getProperty("user.name");
        Properties props=System.getProperties(); //获得系统属性集
        map.put("IP",address.getHostAddress());
        map.put("用户",osUser);//当前用户
        map.put("操作系统名称",props.getProperty("os.name"));//操作系统名称
        map.put("操作系统构架",props.getProperty("os.arch"));//操作系统构架
        map.put("操作系统版本",props.getProperty("os.version"));//操作系统版本
        map.put("操作系统版本",props.getProperty("os.version"));//操作系统版本
        map.put("Java 运行时环境版本",props.getProperty("java.version"));//Java 运行时环境版本
        return map;
    }
}
