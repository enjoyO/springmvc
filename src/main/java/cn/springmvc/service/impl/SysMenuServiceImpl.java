package cn.springmvc.service.impl;

import java.util.List;

import cn.springmvc.dao.SysMenuDao;
import cn.springmvc.model.SysMenu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.springmvc.service.SysMenuService;

@Service
public class SysMenuServiceImpl {

    @Autowired
    private SysMenuDao sysUserDao;

    public String getMenus() {

        StringBuilder menus = new StringBuilder();
        menus.append("<div class='page-sidebar nav-collapse collapse' id='SYS_MENU'>");
        menus.append("<ul class='page-sidebar-menu'>");//菜单开始点
        menus.append("<li><div class='sidebar-toggler hidden-phone'></div></li>");//侧边栏按钮
        //获取所有一级菜单
        List<SysMenu> sysParentMenuList = sysUserDao.getSysMenu(new SysMenu());

        menus.append("<li class='active'><a href='/sys/user/longIn'><span class='title'>系统信息</span><span class='selected'></span></a></li>");

        for (SysMenu menu : sysParentMenuList) {

            //获取二级菜单
            SysMenu sysMenu = new SysMenu();
            sysMenu.setParentId(menu.getMenuId());
            List<SysMenu> sysMenuList = sysUserDao.getSysMenu(sysMenu);
            //无效菜单直接执行下操作
            if(sysMenuList.size() == 0 && menu.getUrl() == null)continue;

            menus.append("<li class=''>");
            if (sysMenuList.size() > 0 && menu.getUrl() == null) {
                menus.append("<a href='javascript:void(0);'>");
            } else {
                menus.append("<a href='javascript:void(0);' onclick=openAct(this) _url='" + menu.getUrl() + "'>");
            }
            menus.append("<i class='icon-bookmark-empty'></i>");
            menus.append("<span class='title'>" + menu.getMenuName() + "</span>");
            if (sysMenuList.size() > 0) {
                menus.append("<span class='arrow'></span>");
            }
            menus.append("</a>");

            if (sysMenuList.size() > 0) {
                menus.append("<ul class='sub-menu'>");
                for (SysMenu m : sysMenuList) {
                    menus.append("<li class=''>");
                    menus.append("<a href='javascript:void(0)' onclick=openAct(this) _url='" + m.getUrl() + "'>" + m.getMenuName() + "</a>");
                    menus.append("</li>");
                }
                menus.append("</ul>");
            }
            menus.append("</li>");
        }
        menus.append("</ul>");//菜单结束点
        menus.append("</div>");
        return menus.toString();
    }
}
