package cn.springmvc.service;

import cn.springmvc.model.SysUser;

/**
 * Created by Lenovo on 2015/9/28.
 */
public interface SysUserService {

    public SysUser find(SysUser sysUser);
}
